from django.urls import path, include
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls')),
    path('rating/',include('rating.urls')),
    path('simulasi/', include('simulasi.urls')),
    path('persentasecovid/', include('statistik.urls')),
    path('feedback/', include('feedback.urls')),
    path('login/',auth_views.LoginView.as_view(template_name='login.html'),name='login'),
    path('logout/',auth_views.LogoutView.as_view(template_name='logout.html'),name='logout'),
    path('signup/',include('register.urls')),
]