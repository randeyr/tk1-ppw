
from django.test import TestCase,Client
from .models import JumlahPendudukAwal, Penduduk
from .views import jumlah,form,savejumlah,savependuduk,sembuhkan,daftarpositif
from django.urls import resolve
from .forms import FormJumlahPenduduk,FormPenduduk

# Create your tests here.

class TestSimulasi(TestCase):
	# test urls
    def test_jumlah_exist(self):
        response = Client().get('/simulasi/jumlah/')
        self.assertEquals(response.status_code,200)

    def test_daftarpositif_exist(self):
        response = Client().get('/simulasi/daftarpositif/')
        self.assertEquals(response.status_code,200)

    def test_form_penduduk_exist(self):
        response = Client().get('/simulasi/form/')
        self.assertEquals(response.status_code,200)

    # test template 
    def test_template_jumlah(self):
    	response = Client().get('/simulasi/jumlah/')
    	self.assertTemplateUsed(response,'jumlah.html')

    def test_template_daftarpositif(self):
    	response = Client().get('/simulasi/daftarpositif/')
    	self.assertTemplateUsed(response,'daftarpositif.html')

    def test_template_form_penduduk(self):
    	response = Client().get('/simulasi/form/')
    	self.assertTemplateUsed(response,'form.html')

    # test view
    def test_kelengkapan_form_jumlah(self):
    	response = Client().get('/simulasi/jumlah/')
    	hfj=response.content.decode('utf8')
    	self.assertIn("Masukan Jumlah Penduduk Mula-Mula",hfj)
    	self.assertIn("Min : 1, Max : 999999",hfj)
    	self.assertIn("Jumlah",hfj)

    def test_kelengkapan_form_penduduk(self):
    	response = Client().get('/simulasi/form/')
    	hfp=response.content.decode('utf8')
    	self.assertIn("Masukan Biodata Warga Baru",hfp)
    	self.assertIn("Nama penduduk",hfp)
    	self.assertIn("Usia",hfp)
    	self.assertIn("Alamat",hfp)
    	self.assertIn("Jenis kelamin",hfp)
    	self.assertIn("Status covid",hfp)

    def test_kelengkapan_daftar_positif(self):
    	response = Client().get('/simulasi/daftarpositif/')
    	hdp=response.content.decode('utf8')
    	self.assertIn("Daftar Positif Covid",hdp)
    	self.assertIn("Daerah X",hdp)
    	self.assertIn("Nama ",hdp)
    	self.assertIn("Usia",hdp)
    	self.assertIn("Alamat",hdp)
    	self.assertIn("Jenis Kelamin",hdp)
    	self.assertIn("Ubah Status",hdp)

    # test model
    def test_model_jumlah(self):
    	JumlahPendudukAwal.objects.create(jumlah=15)
    	jumlah_objek=JumlahPendudukAwal.objects.all().count()
    	self.assertEquals(jumlah_objek,1)

    def test_model_penduduk_gagal(self):
    	Penduduk.objects.create(nama_penduduk="Asep",usia="35", alamat="x", jenis_kelamin="Laki-Laki", status_covid="Negatif")
    	jumlah_daftar=Penduduk.objects.all().filter(status_covid='Positif').count()
    	self.assertEquals(jumlah_daftar,0)

    def test_model_penduduk_berhasil(self):
    	Penduduk.objects.create(nama_penduduk="Asep",usia="35", alamat="x", jenis_kelamin="Laki-Laki", status_covid="Positif")
    	jumlah_daftar=Penduduk.objects.all().filter(status_covid='Positif').count()
    	self.assertEquals(jumlah_daftar,1)

    

    
    

    
    
