# Tugas Kelompok 1 PPW

[![pipeline status][pipeline-badge]][commits-gl]

## Daftar isi

- [Daftar isi](#daftar-isi)
- [Anggota Kelompok](#anggota-kelompok)
- [Cerita Aplikasi](#cerita-aplikasi)
- [Fitur-fitur Aplikasi](#fitur-fitur-aplikasi)
- [Link Aplikasi](#link-aplikasi)

## Kelompok C10
Nama Anggota :
- Ahmad Randey Juliano Rasyid - 1906399285
- Garry Hanuga - 1906399184
- Ahsan Najmy Ramadhan Putra Aji - 1906398641
- Aji Putra Pamungkas - 1906398736

## Cerita Aplikasi

**Simulasi COVID 19**

Simulasi COVID 19 adalah aplikasi Role Playing bertema pandemi [COVID-19][covid-19]. Aplikasi ini memisalkan kita sebagai pengunjung website atau pemakai aplikasi menjadi seorang pengurus warga daerah X yang berkewajiban untuk mendata setiap pengunjung desa X. 

COVID-19 merupakan virus mematikan yang dapat menular pada orang-orang di sekitar. Menurut survey, sebagian besar (sekitar 80%) orang yang terinfeksi berhasil pulih tanpa perlu perawatan khusus, dan sekitar 1 dari 5 orang yang terinfeksi COVID-19 menderita sakit parah dan kesulitan bernapas. Lansia dan orang-orang kondisi medis seperti tekanan darah tinggi, gangguan jantung dan paru-paru, diabetes, atau kanker memiliki kemungkinan lebih besar mengalami sakit lebih serius. Namun, siapa pun dapat terinfeksi COVID-19 dan mengalami sakit yang serius. 

Mula-mula semua warga desa X tidak ada yang terinfeksi COVID-19, sehingga untuk menghindari penularan COVID 19 yang bisa meresahkan warga sekitar, kita sebagai pengurus warga harus mendata semua warga baru/pengunjung baik itu nama, jenis kelamin, usia, alamat dimana dia akan tinggal di daerah X, serta status COVID 19 apakah dia terinfeksi atau tidak. Apabila dia positif, maka kamu diwajibkan untuk mengarantina dia supaya warga desa tidak ada yang terinfeksi. Orang tersebut bisa sembuh bisa tidak. Terakhir akan dihitung berapa persentase warga daerah X yang positif COVID 19, termasuk warga baru/pengunjung.

## Fitur-fitur Aplikasi
1. Tambah warga : 
   Fitur ini meruapkan form dimana kita memasukan data warga baru/pengunjung yang akan tinggal di daerah X

2. Data positif covid pengunjung desa X : 
   Fitur ini menampilkan semua pengunjung daerah X berstatus positif dengan data pribadi mereka. Terdapat juga fitur "sembuhkan" untuk menghapus seseorang yang berstatus positif dari Daftar Positif.

3. Presentase positif COVID-19 di desa X : 
   Fitur ini menampilkan presentase orang yang memiliki status COVID-19 positif di dalam daerah. 

4. Navigation bar :
   Memudahkan pengguna berpindah halaman dengan cepat

5. Fitur “Coba Lagi” :
   Fitur ini akan mereset data warga dari awal, dan memulai simulasi dari nol kembali

6. Fitur "Rating" :
   Meminta input rating user berdasarkan kualitas website kami dan menampilkan semua rating di halaman rating.

7. Fitur "Feedback" :
   Meminta feedback dari user mengenai user experience dari website kami dan/atau saran dan menampilkan feedback tersebut di halaman feedback.

## Link Aplikasi

[https://c19simulation.herokuapp.com][link-heroku]


[pipeline-badge]: https://gitlab.com/randeyr/tk1-ppw/badges/master/pipeline.svg
[commits-gl]: https://gitlab.com/randeyr/tk1-ppw/-/commits/master
[covid-19]: https://www.who.int/indonesia/news/novel-coronavirus/qa-for-public
[link-heroku]: https://c19simulation.herokuapp.com/