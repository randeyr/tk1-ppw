from django import forms
from . import models


class input_nama(forms.ModelForm):
    class Meta:
        model = models.feedback
        fields = ['nama']
    input_attrs1 = {
		'type' : 'text',
		'placeholder' : 'Masukkan namamu disini..',
        'class' : 'textp'
	}
    nama = forms.CharField(label='', max_length=100, required = False, widget=forms.TextInput(attrs=input_attrs1))


class input_pesan(forms.ModelForm):
    class Meta:
        model = models.feedback
        fields = ['pesan']
    error_messages = {
		'required' : 'Please fill'
	}
    input_attrs1 = {
		'rows' : 7,
        'class' : 'form-control textp',
        'placeholder' : 'Masukkan pesanmu disini..'

	}
    pesan = forms.CharField(label='', max_length=99999, widget=forms.Textarea(attrs=input_attrs1))

