from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import listfeedback, tambah, saveform
from .models import feedback

class feedbackTest(TestCase):
    def feedback_urls_is_exist(self):
        response = Client().get('/feedback/')
        self.assertEqual(response.status_code,200)

    def feedback_templates_is_exist(self):
        response = Client().get('/feedback/')
        self.assertTemplateUsed(response, 'feedback.html')
    
    def feedback_using_func(self):
        found = resolve('/feedback/')
        self.assertEqual(found.func, listfeedback)

    def tambah_urls_is_exist(self):
        response = Client().get('/feedback/tambah/')
        self.assertEqual(response.status_code, 200)

    def tambah_templates_is_exist(self):
        response = Client().get('/feedback/tambah')
        self.assertTemplateUsed(response, 'tambah.html')
    
    def tambah_using_func(self):
        found = resolve('/feedback/tambah')
        self.assertEqual(found.func, tambah)

    def saveform_url_is_exist(self):
        response = Client().get('/feedback/saveform/')
        self.asserEqual(response.status_code, 302)
    

    def test_model_can_create_new_activity(self):
        new_feeds = feedback.objects.create(nama="a",pesan="b",img="1",num="2")
        new_feeds.save()
        count_all = feedback.objects.all().count()
        self.assertEqual(count_all, 1)

    def test_can_save_POST_request(self):
        response = self.client.post('/feedback/saveform/', data={'nama' : 'a', 
            'pesan' : 'b','img':'/static/1.png'})
        count_all = feedback.objects.all().count()
        self.assertEqual(count_all, 1)

        self.assertEqual(response.status_code,302)
        self.assertEqual(response['location'], '/feedback/')
        
    def test_can_save_POST_anonymous(self):
        response = self.client.post('/feedback/saveform/', data={'nama' : '',
            'pesan' : 'b', 'img' : '1'})
        count_based_on_name = feedback.objects.filter(nama = 'Anonim').count()
        self.assertEqual(count_based_on_name, 1)

