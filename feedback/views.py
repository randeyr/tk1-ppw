from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import feedback
from .forms import input_nama, input_pesan
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def listfeedback(request):
    response={
        'feedbacks' : feedback.objects.all(),
    }
    return render(request, 'feedback.html', response)

@login_required
def tambah(request):
    response={
        'form' : input_nama,
        'form2' : input_pesan,
    }
    return render(request, 'tambah.html', response)

@login_required
def saveform(request):
    if request.method == 'POST':
        form = input_nama(request.POST)
        form2 = input_pesan(request.POST)
        pimg = request.POST.get('img-picker')
        if form.is_valid() & form2.is_valid():
            if form.cleaned_data['nama'] :
                name = form.cleaned_data['nama']
            else : name = "Anonim"
            pesen = form2.cleaned_data['pesan']
            simg = switchimg(pimg)
            nom = len(feedback.objects.all()) + 1
            feedbacks = feedback(nama = name, pesan = pesen, img=simg, num = nom)
            feedbacks.save()
            return HttpResponseRedirect('/feedback/')
        else:
            return HttpResponseRedirect('/feedback/')

@login_required
def switchimg(arg):
    switcher = {
        '1': "{% static '1.png' %}",
        '2': "{% static '2.png' %}",
        '3': "{% static '3.png' %}",
        '4': "{% static '4.png' %}",
        '5': "{% static '5.png' %}",
        '6': "{% static '6.png' %}",
        '7': "{% static '7.png' %}",
        '8': "{% static '8.png' %}",
    }
    urlimg = switcher.get(arg, lambda: "invalid")
    return urlimg
