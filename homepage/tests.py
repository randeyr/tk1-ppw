from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import index

class indexTest(TestCase):
    def test_index_urls_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_index_templates(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_index_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    
