from django.shortcuts import render
from .models import Akun
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def halaman_form(request):

    return render(request,'halaman_form.html')

@login_required
def feedback_form(request):
    args = {}
    username = request.POST.get('username','test 1')
    rating = request.POST.get('inlineRadioOptions','5')

    user_rating = Akun(username=username,rating=rating)
    user_rating.save()
    tampilan = "Rating anda berhasil tersimpan"
    args['sukses'] = tampilan
    return render(request,'halaman_form.html',args)

@login_required
def halaman_rating(request):
    info_rating = Akun.objects.all()
    rating = {
        "info_rating" : info_rating
    }
    return render(request,'halaman_rating.html',rating)

