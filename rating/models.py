from django.db import models

# Create your models here.
class Akun(models.Model):
    username = models.CharField(max_length=200)

    OPTION1 = '1'
    OPTION2 = '2'
    OPTION3 = '3'
    OPTION4 = '4'
    OPTION5 = '5'

    RATING = (
        (OPTION1,'1'),
        (OPTION2,'2'),
        (OPTION3,'3'),
        (OPTION4,'4'),
        (OPTION5,'5'),
    )
    rating = models.CharField(max_length=2, choices=RATING,default=OPTION5,null=True)