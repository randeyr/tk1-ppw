from django.test import TestCase
from django.test import Client
from simulasi.models import JumlahPendudukAwal
from simulasi.models import Penduduk

class ActivityTest(TestCase):
    
    def test_url_statistik(self):
        response = Client().get('/persentasecovid/')
        self.assertEqual(200, response.status_code)

    def test_template_statistik(self):
        response = Client().get('/persentasecovid/')
        self.assertTemplateUsed(response, 'stats.html')

    def test_halaman_statistik(self):
        response = Client().get('/persentasecovid/')
        fields = response.content.decode('utf8')
        self.assertIn("Persentase", fields)
        self.assertIn("Positif", fields)
        self.assertIn("Coba Lagi", fields)