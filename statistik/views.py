from django.shortcuts import render, redirect
from simulasi.models import JumlahPendudukAwal
from simulasi.models import Penduduk
from django.contrib.auth.decorators import login_required

@login_required
def statCovid(request):

    persen_positif = 0
    persen_negatif = 0
    persen_pria = 0
    persen_perempuan = 0
    persen_muda = 0
    persen_dewasa = 0
    persen_tengah = 0
    persen_tua = 0

    jumlah_awal = JumlahPendudukAwal.objects.all().count()

    if JumlahPendudukAwal.objects.all().count() > 0 and Penduduk.objects.filter(status_covid='Positif').count() == 0:
        persen_negatif = 100

    elif Penduduk.objects.filter(status_covid='Positif').count() > 0:
        
        awal = JumlahPendudukAwal.objects.all()[0]
        total = awal.jumlah + Penduduk.objects.all().count()
        
        # statistik berdasarkan status covid
        jumlah_positif = Penduduk.objects.filter(status_covid='Positif').count()
        persen_positif = jumlah_positif * 100 // total
        persen_negatif = 100 - persen_positif

        # statistik berdasarkan jenis kelamin
        jumlah_pria = Penduduk.objects.filter(status_covid='Positif', jenis_kelamin='Laki-Laki').count()
        persen_pria = jumlah_pria * 100 // jumlah_positif
        persen_perempuan = 100 - persen_pria

        # statistik berdasarkan usia
        usiaMuda = 0
        usiaDewasa = 0
        usiaTengah = 0
        usiaTua = 0

        for penduduk in Penduduk.objects.filter(status_covid='Positif'):
            if int(penduduk.usia) <= 20:
                usiaMuda += 1

            elif int(penduduk.usia) > 20 and int(penduduk.usia) < 45:
                usiaDewasa += 1

            elif int(penduduk.usia) >= 45 and int(penduduk.usia) <= 65:
                usiaTengah += 1

            else:
                usiaTua +=1
        
        persen_muda = usiaMuda * 100 // jumlah_positif
        persen_dewasa = usiaDewasa * 100 // jumlah_positif
        persen_tengah = usiaTengah * 100 // jumlah_positif
        persen_tua = usiaTua * 100 // jumlah_positif

    response = {
        'jumlahawal': jumlah_awal,
        'positif': persen_positif,
        'negatif': persen_negatif,
        'pria': persen_pria,
        'perempuan': persen_perempuan,
        'muda': persen_muda,
        'dewasa': persen_dewasa,
        'tengah': persen_tengah,
        'tua': persen_tua
    }
    return render(request, 'stats.html', response)

@login_required
def cobaLagi(request):
    JumlahPendudukAwal.objects.all().delete()
    Penduduk.objects.all().delete()

    return redirect('/')